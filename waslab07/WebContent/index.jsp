<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Wall of Tweets 07</title>
<link href="wallstyle.css" rel="stylesheet" type="text/css" />
</head>
<body class="wallbody">
	<h1 class="walltitle">Wall of Tweets 07</h1>
	<%
		if (session.getAttribute("authorization_flow") == null) {
	%>
	<div class="walltweet">
		<p><a href="rest/oauth">Sign In with Twitter</a></p>
	</div>
	<br>
	<%
		} else if (session.getAttribute("screen_name") == null) {
	%>
	<div class="walltweet">
		<p>You've just completed task#3<p>
	</div>
	<br>
	<%
		} else {
	%>
	<jsp:include page="tweets.html" />
	<%
		}
	%>
</body>
</html>