var baseURI = "rest";
var wallURI = baseURI+"/tweets";
var req;
var tweetBlock = "	<div id='tweet_{0}' class='wallitem'>\n\
	<div class='likes'>\n\
	<span class='numlikes'>{1}</span><br /> <span\n\
	class='plt'>people like this</span><br /> <br />\n\
	<button onclick='{5}Handler(\"{0}\")'>{5}</button>\n\
	<br />\n\
	</div>\n\
	<div class='item'>\n\
	<h4>\n\
	<em>{2}</em> on {4}\n\
	</h4>\n\
	<p>{3}</p>\n\
	</div>\n\
	</div>\n";

String.prototype.format = function() {
	var args = arguments;
	return this.replace(/{(\d+)}/g, function(match, number) { 
		return typeof args[number] != 'undefined'
			? args[number]
		: match
		;
	});
};

function likeHandler(tweetID) {
	var target = 'tweet_' + tweetID;
	var uri = wallURI+ "/" + tweetID +"/like";
	req = new XMLHttpRequest();
	req.open('GET', uri, /*async*/true);
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				document.getElementById(target).getElementsByClassName("numlikes")[0].innerHTML = req.responseText;
			}
			else {
				alert(req.status+": "+req.statusText);
				window.location.reload();
			}
		}
	};
	req.send(/*no params*/null);
}

function deleteHandler(tweetID) {
	req = new XMLHttpRequest(); 
	var delURI = wallURI+ "/" + tweetID;
	req.open("DELETE", delURI, true); 
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				var element = document.getElementById("tweet_"+tweetID);
				element.parentNode.removeChild(element);
				localStorage.removeItem("tweet_" + tweetID);
			}
			else {
				alert(req.status+": "+req.statusText);
				window.location.reload();
			}
		}
	};
	req.send(null); 
}

function getTweetHTML(tweet, action) {  // action :== "like" xor "delete"
	var dat = new Date(tweet.time);
	var dd = dat.toDateString()+" @ "+dat.toLocaleTimeString();
	return tweetBlock.format(tweet.tweet_id, tweet.likes, tweet.user_name, tweet.text, dd, action);

}

function getTweets() {
	req = new XMLHttpRequest(); 
	req.open("GET", wallURI, true); 
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				var tweet_list = "";
				var tweets = JSON.parse(req.responseText);
				for (var i=0; i<tweets.length; i++) {
					var tt = tweets[i];
					var key = "tweet_" + tt.tweet_id;
					var action = "like";
					if (localStorage[key]) action = "delete";
					tweet_list += getTweetHTML(tt, action);
				};
				document.getElementById("tweet_list").innerHTML = tweet_list;
			}
			else {
				alert(req.status+": "+req.statusText);
				window.location.reload();
			}
		}
	};
	req.send(null); 
};


function tweetHandler() {
	var text = document.getElementById("tweet_text").value;

	req = new XMLHttpRequest(); 
	req.open("POST", wallURI, true); 
	req.onreadystatechange = function() {
		if (req.readyState == 4) {
			if (req.status == 200) {
				var tweet_list = document.getElementById("tweet_list").innerHTML;
				var newtweet = JSON.parse(req.responseText);
				tweet_list = getTweetHTML(newtweet, "delete") + tweet_list;
				document.getElementById("tweet_list").innerHTML = tweet_list;
				localStorage.setItem("tweet_"+newtweet.tweet_id, req.responseText);

				// clear form fields
				document.getElementById("tweet_text").value = "";
			}
			else {
				alert(req.status+": "+req.statusText);
				window.location.reload();
			}
		}
	};
	var tweet = {text: text};
	req.setRequestHeader("Content-Type","application/json");
	req.send(JSON.stringify(tweet)); 
};

//main
function main() {
	document.getElementById("tweet_submit").onclick = tweetHandler;
	getTweets();
};
