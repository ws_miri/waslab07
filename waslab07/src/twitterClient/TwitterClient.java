/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2010-2014 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * http://glassfish.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package twitterClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Feature;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.oauth1.ConsumerCredentials;
import org.glassfish.jersey.client.oauth1.OAuth1AuthorizationFlow;
import org.glassfish.jersey.client.oauth1.OAuth1ClientSupport;

import com.google.appengine.repackaged.org.json.JSONArray;
import com.google.appengine.repackaged.org.json.JSONObject;

/** Simple command-line application that uses Jersey OAuth client library to authenticate
 * with Twitter.
 *
 * @author Martin Matula
 * @author Miroslav Fuksa (miroslav.fuksa at oracle.com)
 */
public class TwitterClient {
	private static final BufferedReader IN = new BufferedReader(new InputStreamReader(System.in, Charset.forName("UTF-8")));
	private static final String FRIENDS_TIMELINE_URI = "https://api.twitter.com/1.1/statuses/home_timeline.json";

	private static final String CONSUMER_KEY = "YOUR CONSUMER KEY";
	private static final String CONSUMER_SECRET = "YOUR CONSUMER SECRET";

	/**
	 * Main method that creates a {@link Client client} and initializes the OAuth support with
	 * configuration needed to connect to the Twitter and retrieve statuses.
	 * <p/>
	 * Execute this method to demo
	 *
	 * @param args Command line arguments.
	 * @throws Exception Thrown when error occurs.
	 */

	public static void main(final String[] args) throws Exception {



		final ConsumerCredentials consumerCredentials = new ConsumerCredentials(CONSUMER_KEY, CONSUMER_SECRET);


		// we do not have Access Token yet. Let's perfom the Authorization Flow first,
		// let the user approve our app and get Access Token.
		final OAuth1AuthorizationFlow authFlow = OAuth1ClientSupport.builder(consumerCredentials)
				.authorizationFlow(
						"https://api.twitter.com/oauth/request_token",
						"https://api.twitter.com/oauth/access_token",
						"https://api.twitter.com/oauth/authorize")
						.build();
		final String authorizationUri = authFlow.start();

		System.out.println("Enter the following URI into a web browser and authorize me:");
		System.out.println(authorizationUri);
		System.out.print("Enter the authorization code: ");
		final String verifier;
		try {
			verifier = IN.readLine();
		} catch (final IOException ex) {
			throw new RuntimeException(ex);
		}
		authFlow.finish(verifier);

		// get the feature that will configure the client with consumer credentials and
		// received access token
		final Feature filterFeature = authFlow.getOAuth1Feature();

		// create a new Jersey client and register filter feature that will add OAuth signatures 
		final Client client = ClientBuilder.newBuilder()
				.register(filterFeature)
				.build();

		// make requests to protected resources
		// (no need to care about the OAuth signatures)
		final Response response = client.target(FRIENDS_TIMELINE_URI).request().get();
		if (response.getStatus() != 200) {
			String errorEntity = null;
			if (response.hasEntity()) {
				errorEntity = response.readEntity(String.class);
			}
			throw new RuntimeException("Request to Twitter was not successful. Response code: "
					+ response.getStatus() + ", reason: " + response.getStatusInfo().getReasonPhrase()
					+ ", entity: " + errorEntity);
		}

		String output = response.readEntity(String.class);
		JSONArray result = new JSONArray(output);
		System.out.println("Tweets:");
		for (int i = 0; i < result.length(); i++) 
		{
			JSONObject s = result.getJSONObject(i);
			System.out.println(" "+ s.getString("created_at")+ ": "+ s.getString("text"));
		}
	}
}
