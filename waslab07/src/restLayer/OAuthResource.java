package restLayer;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.oauth1.OAuth1AuthorizationFlow;

import businessLayer.models.ModelException;
import businessLayer.models.User;

@Path("/oauth")
public class OAuthResource {

	private static final String CONSUMER_KEY = "YOUR CONSUMER KEY";
	private static final String CONSUMER_SECRET = "YOUR CONSUMER SECRET";

	@GET
	public Response oauth(@Context HttpServletRequest httpRequest) throws URISyntaxException {

		OAuth1AuthorizationFlow authFlow = null;
		String authorizationUri = null;

		/*
		 * Write your code here. Assign the proper values to service and requestToken
		 */


		//

		httpRequest.getSession().setAttribute("authorization_flow",authFlow);
		return Response.seeOther(new URI(authorizationUri)).build();
	}

	@GET
	@Path("/redirect")
	public Response redirect(@QueryParam("oauth_verifier") String oauthVerifier, @Context HttpServletRequest httpRequest) throws URISyntaxException {


		String screen_name = null;
		OAuth1AuthorizationFlow authFlow = (OAuth1AuthorizationFlow) httpRequest.getSession().getAttribute("authorization_flow");

		/*
		 * Write your code here. Use the request GET https://api.twitter.com/1.1/account/verify_credentials.json
		 * to obtain the screen_name of the authenticated user
		 */


		//

		try {
			if (screen_name != null) {
				if (User.findByName(screen_name) == null)
					User.create(screen_name, "default_password");
				httpRequest.getSession().setAttribute("screen_name", screen_name);
			}
		} 
		catch (ModelException exx) {
			throw new WebApplicationException(exx, Response.Status.INTERNAL_SERVER_ERROR);
		}

		return Response.seeOther(new URI("http://localhost:8080/waslab07/")).build();
	}

}
