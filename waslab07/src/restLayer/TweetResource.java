package restLayer;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import businessLayer.models.ModelException;
import businessLayer.models.Tweet;
import businessLayer.models.User;

@Path("/tweets")
public class TweetResource {

	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Collection<Tweet> findAll() {
		try {
			return Tweet.findAll();
		} catch (ModelException e) {
			throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	@GET @Path("{id}/like")
	@Produces({ MediaType.TEXT_PLAIN})
	public String like(@PathParam("id") Integer id) {
		try {
			Tweet twt = Tweet.findById(id);
			Integer likes = twt.getLikes() + 1;
			twt.setLikes(likes);
			twt.update();
			return likes.toString();
		} catch (ModelException e) {
			throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON})
	@Produces({ MediaType.APPLICATION_JSON})
	public Tweet create(Tweet posted,  @Context HttpServletRequest request) {

		String username = (String) request.getSession().getAttribute("screen_name");

		try {
			if (username != null) {
				User user = User.findByName(username);
				if (user != null) {

					/* Task #5: insert here your code to publish your tweets in Twitter */

					//
					Tweet tweet = Tweet.create(user.getUser_id(), posted.getText());
					return tweet;
				}
			}
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);

		} catch (ModelException e) {
			throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

	@DELETE @Path("{id}")
	@Produces({ MediaType.TEXT_PLAIN })
	public String remove(@PathParam("id") int id, @Context HttpServletRequest request) {

		String username = (String) request.getSession().getAttribute("screen_name");
		try {
			if (username != null) {
				User user = User.findByName(username);
				if (user != null){
					Tweet tweet = Tweet.findById(id);
					if (tweet != null && tweet.getUser_id() == user.getUser_id()) {
						tweet.delete();
						return ""+id;
					}
				}
			}
			throw new WebApplicationException(Response.Status.UNAUTHORIZED);
		}
		catch (ModelException e) {
			throw new WebApplicationException(e, Response.Status.INTERNAL_SERVER_ERROR);
		}
	}

}
